import React from "react";
import "./JobDetail.css";
import { connect } from "react-redux";

const JobDetail = (props) => {
  if (!props.selectedJob) {
    return <h2></h2>;
  }
  return (

    <div className="jobDetailCard"
    >
      <div className="jobDetailsContent">
        <p
          style={{
            fontWeight: "5000px",
            fontSize: "20px",
            paddingTop: "10px",
          }}
        >
          {props.selectedJob.name}
        </p>
        <p
          style={{ fontSize: "25px", paddingTop: "0px", marginBottom: "-50px"}}
        >
          {props.selectedJob.location.city},{" "}
          {props.selectedJob.location.country}
        </p>
        <img
          src={props.selectedJob.logo}
          alt="logo"
          style={{ width: "50%", alignSelf: "center" , marginTop: "50px"}}
        />

        <p
          style={{
            marginTop: "60px",
            marginBottom: "0px",
            fontSize: "15px",
            paddingTop: "20px",
          }}
        >
          {props.selectedJob.description}
        </p>
        <p>
          <span style={{ fontWeight: "500" }}>
            Salary:{props.selectedJob.salary / 1000}K
          </span>
        </p>
        <div
          style={{
            justifyContent: "center",
            alignSelf: "center",
            flexDirection: "row",
            display: "flex",
          }}
        >
          <button className="btn_Apply">Apply</button>
          <button className="btn_Cancel">Not Intrested</button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return { selectedJob: state.selectJob };
};

export default connect(mapStateToProps)(JobDetail);
